#/bin/bash
#Kontrollib, kas Samba on olemas, kui ei ole siis paigaldab
type smbd > /dev/null 2>&1 || apt-get update > /dev/null 2>&1 && apt-get -y install samba > /dev/null 2>&1

#Kontrollib, kas kaust on olemas, kui ei ole siis loob
test -d $KAUST || mkdir -p $KAUST || exit

#Kontrollib, kas grupp on olemas, kui ei ole siis loob
getent group $GRUPP > /dev/null || addgroup $GRUPP > /dev/null

#Muudab KAUST gruppi
#chgrp $GRUPP $KAUST

#Annab grupile kirjutamisoigused
#chmod g+w $KAUST

#Kirjutab Samba konfiguratsioonifaili uued parameetrid
cat  >> /etc/samba/smb.conf << EOF
[lab]
path=$KAUST
writable=yes
valid users=@$GRUPP
force group=$GRUPP
browsable=yes
create mask=0664
directory mask=0775

EOF

exit 0
